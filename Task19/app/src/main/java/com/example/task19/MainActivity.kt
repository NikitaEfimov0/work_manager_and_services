package com.example.task19

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkRequest
import com.google.android.material.floatingactionbutton.FloatingActionButton


class MainActivity : AppCompatActivity() {
    var show:FloatingActionButton? = null
    var recView:RecyclerView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        show = findViewById(R.id.showData)
        recView = findViewById(R.id.recView)
        val linearLayoutManager = LinearLayoutManager(this)
        recView?.layoutManager = linearLayoutManager


            Thread {
                while (true) {
                try {
                    Thread.sleep(2000)
                } catch (e: InterruptedException) {
                }
                (this as Activity).runOnUiThread {
                    mainWork()
                    Log.d("WORKING", "WORKING WAS DONE SUCCESSFULLY")
                }
                }
            }.start()

    }

    private fun mainWork() {
        val mRequest:WorkRequest = OneTimeWorkRequestBuilder<MyWorker>().build()
        WorkManager.getInstance(this).enqueue(mRequest)
        show?.setOnClickListener {
            recView?.adapter = CurrentAdapter.mAdapter
        }
    }
}