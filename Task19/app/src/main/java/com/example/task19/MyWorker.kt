package com.example.task19

import android.content.Context
import android.content.Intent
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.task18.MyDataItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MyWorker (context: Context, workerParameters: WorkerParameters):Worker(context, workerParameters){
    val BASE_URL = "https://jsonplaceholder.typicode.com/"
    var myAdapter:MyAdapter? = null
    override fun doWork(): Result {
        Intent(applicationContext, MyService::class.java).also {
            startService(it)

        }

        return Result.success()
    }

    private fun startService(it: Intent) {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(APIInterface::class.java)

        val retrofitData = retrofitBuilder.getData()

        retrofitData.enqueue(object : Callback<List<MyDataItem>?> {
            override fun onResponse(
                call: Call<List<MyDataItem>?>?,
                response: Response<List<MyDataItem>?>?
            ) {
                val responseBody = response?.body()!!

                myAdapter = MyAdapter(responseBody)
                myAdapter?.notifyDataSetChanged()
                CurrentAdapter.mAdapter = myAdapter


            }

            override fun onFailure(call: Call<List<MyDataItem>?>?, t: Throwable?) {

            }
        })
    }


}